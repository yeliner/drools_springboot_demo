package com.qyl.demo.services;

import com.alibaba.fastjson.JSONObject;
import com.qyl.demo.dao.PromoteMapper;
import com.qyl.demo.model.Promote;
import com.qyl.demo.model.RuleResultVo;
import com.qyl.demo.util.DrlExecute;
import com.qyl.demo.util.UUIDUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author qiuyelin
 * @version 1.0
 * @Description: TODO
 * @date 2021/7/20 13:43
 */
@Service
public class PromoteService {
    @Resource
    private PromoteMapper promoteMapper;

    /*
     * 生成优惠券
     * */
    public String editPromote(String money, String ruleName) {
        Promote promote = new Promote();
        double v = Double.parseDouble(money);
        // 根据模板引擎生成规则内容
        String rule = UUIDUtil.rule(ruleWorkMap(ruleName, v));
        // 生成编码
        String promoteCode = UUIDUtil.typeJoinTime();
        promote.setPromoteCode(promoteCode);
        promote.setPromoteName(ruleName);
        promote.setPromoteRule(rule);
        // 优惠券保存到数据库
        int i = promoteMapper.insertPromote(promote);
        return i > 0 ? "success" : "fail";
    }

    /*
     * 购物车计算
     * */
    public Map<String, Object> calcPromote(String moneySum) {
        Map<String, Object> map = new HashMap<>();
        // 优惠后价格
        double afterMoney = Double.parseDouble(moneySum);
        // 查询优惠券
        List<Promote> promoteList = queryPromote();
        // 优惠后结果
        List<RuleResultVo> resultVoList = new ArrayList<>();
        if (promoteList.size() > 0) {
            // 说明有优惠券
            for (Promote promote : promoteList
            ) {
                // 匹配优惠券规则
                RuleResultVo ruleResultVo = DrlExecute.rulePromote(promote, afterMoney);
                afterMoney = ruleResultVo.getFinallyMoney();
                resultVoList.add(ruleResultVo);
            }
        }
        map.put("before", moneySum);
        map.put("after", afterMoney);
        map.put("list", resultVoList);
        return map;
    }

    /*
     * 查询优惠券
     * */
    private List<Promote> queryPromote() {
        return promoteMapper.queryAll();
    }

    /*
     * 组合业务规则Json方法
     * */
    private String ruleWorkMap(String name, Double money) {
        Map<String, Object> map = new HashMap<>();

        // Rule部分
        Map<String, Object> rule = new HashMap<>();
        rule.put("name", name);
        // When部分
        Map<String, Object> when = new HashMap<>();
        // then部分
        Map<String, Object> then = new HashMap<>();
        then.put("money", money);
        //组合规则 rule when and then
        map.put("rule", rule);
        map.put("condition", when);
        map.put("action", then);
        return JSONObject.toJSONString(map);
    }
}
