package com.qyl.demo.util;

import org.kie.api.KieBase;
import org.kie.api.io.Resource;
import org.kie.api.io.ResourceType;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.utils.KieHelper;

import java.io.StringReader;

/**
 * @author qiuyelin
 * @version 1.0
 * @Description: TODO 初始化规则库
 * @date 2021/7/20 14:25
 */
public class KieUtil {

    // 初始化规则库 是Kie的一个工具类
    public static KieBase ruleKieBase(String rule, String name) {
        KieHelper kieHelper = new KieHelper();
        try {
            kieHelper.kfs.write(generateResourceName(ResourceType.DRL, name), rule);
            return kieHelper.build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(" drools init failed!");
        }
    }

    private static String generateResourceName(ResourceType type, String name) {
        // 这里的路径对应模板引擎中的package包路径
        return "src/main/resources/rules/" + name + "." + type.getDefaultExtension();
    }


}
