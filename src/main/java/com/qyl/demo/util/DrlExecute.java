package com.qyl.demo.util;

import com.qyl.demo.model.Promote;
import com.qyl.demo.model.RuleResultVo;
import org.kie.api.KieBase;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.command.CommandFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author qiuyelin
 * @version 1.0
 * @Description: TODO 规则工具类
 * @date 2021/7/20 14:41
 */
public class DrlExecute {
    protected static Logger logger = LoggerFactory.getLogger(DrlExecute.class);

    /*
     * 匹配优惠券规则
     * */
    public static RuleResultVo rulePromote(Promote promote, Double beforeMoney) {
        RuleResultVo resultVo = new RuleResultVo();
        resultVo.setBeforeMoney(beforeMoney);
        logger.info("优惠前价格" + beforeMoney);
        // 命令执行对象
        List cmdCondition = new ArrayList<>();
        cmdCondition.add(CommandFactory.newInsert(resultVo));
        // 初始化规则库
        KieBase KieBase = KieUtil.ruleKieBase(promote.getPromoteRule(), promote.getPromoteCode());
        // 无状态KieSession
        StatelessKieSession kieSession = KieBase.newStatelessKieSession();
        // 调用规则
        kieSession.execute(CommandFactory.newBatchExecution(cmdCondition));
        logger.info("优惠后的价格" + resultVo.getFinallyMoney());
        return resultVo;
    }

}
