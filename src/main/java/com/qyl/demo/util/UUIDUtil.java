package com.qyl.demo.util;

import com.alibaba.fastjson.JSONObject;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupString;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author qiuyelin
 * @version 1.0
 * @Description: TODO
 * @date 2021/7/20 15:40
 */
public class UUIDUtil {
    /*
     * 生成不重复时间编码
     * */
    public static String typeJoinTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String dateNowStr =sdf.format(new Date());
        Integer math = (int) ((Math.random() * 9 + 1) * 1000000);
        return dateNowStr.concat(math.toString());
    }


    public static String rule(String json) {
        return ruleWordExchangeST(json);
    }

    /*
     * 生成规则业务内容
     * */
    private static String ruleWordExchangeST(String json) {
        STGroup group = new STGroupString(workMoneyST);
        ST stFile = group.getInstanceOf("wordImport");
        ST stRule = group.getInstanceOf("ruleValue");
        JSONObject jsonObject = JSONObject.parseObject(json);
        JSONObject condition = jsonObject.getJSONObject("condition");
        JSONObject action = jsonObject.getJSONObject("action");
        JSONObject rule = jsonObject.getJSONObject("rule");
        stRule.add("condition", condition);
        stRule.add("action", action);
        stRule.add("rule", rule);
        stFile.add("ruleStr", stRule);
        return stFile.render();

    }


    //通过传参数 即可生成规则内容，将规则内容放在数据库或征程规则文件，为规则库的构建设定基础
    private static String workMoneyST="wordImport(ruleStr)::=<<\n" +
            "    package rules\n" +
            "    import com.qyl.demo.model.RuleResultVo;\n" +
            "    <ruleStr;separator=\"\\n\">\n" +
            ">>\n" +
            "ruleValue(condition,action,rule)::=<<\n" +
            "    rule \"<rule.name>\"\n" +
            "    no-loop true\n" +
            "        when\n" +
            "            $r:RuleResultVo(true)\n" +
            "        then\n" +
            "            modify($r){\n" +
            "                setPromoteName(\"<rule.name>\")\n" +
            "                 <if(action)>,\n" +
            "                    setFinallyMoney($r.getBeforeMoney()-<action.money> <endif>)\n" +
            "           }\n" +
            "    end\n" +
            ">>";
}
