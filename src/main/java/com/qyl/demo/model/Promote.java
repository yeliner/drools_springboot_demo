package com.qyl.demo.model;

import lombok.Data;

/**
 * @author qiuyelin
 * @version 1.0
 * @Description: TODO 存放规则编码、规则姓名、规则内容等
 * @date 2021/7/20 13:49
 */
@Data
public class Promote {
    // 促销编号
    private String promoteCode;
    // 促销名称
    private String promoteName;
    // 规则内容
    private String promoteRule;
}
