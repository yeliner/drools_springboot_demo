package com.qyl.demo.model;

import lombok.Data;

/**
 * @author qiuyelin
 * @version 1.0
 * @Description: TODO 返回值实体
 * @date 2021/7/20 14:35
 */
@Data
public class RuleResultVo {
    // 参加活动商品优惠后的价格
    private double finallyMoney;
    // 参加优惠前价格
    private double beforeMoney;
    // 参加活动的名称
    private String promoteName;
}
