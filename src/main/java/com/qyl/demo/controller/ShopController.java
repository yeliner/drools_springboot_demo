package com.qyl.demo.controller;

import com.qyl.demo.services.PromoteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author qiuyelin
 * @version 1.0
 * @Description: TODO
 * @date 2021/7/20 13:42
 */
@Controller
@RequestMapping("/promote")
public class ShopController {
    @Resource
    private PromoteService promoteService;

    @RequestMapping("/index")
    public String index() {
        return "index";
    }

    @RequestMapping("/shopping")
    public String shopping() {
        return "shopping";
    }

    /*
     * 添加优惠券
     * */
    @RequestMapping(value = "/addPromote", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String editPromote(String money, String ruleName) {
        return promoteService.editPromote(money, ruleName);
    }

    /*
     * 购物车计算
     * */
    @RequestMapping(value = "/calcPromote", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Map<String, Object> calcPromote(String moneySum) {
        return promoteService.calcPromote(moneySum);
    }
}