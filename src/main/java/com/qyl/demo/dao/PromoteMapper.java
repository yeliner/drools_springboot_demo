package com.qyl.demo.dao;

import com.qyl.demo.model.Promote;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author qiuyelin
 * @version 1.0
 * @Description: TODO
 * @date 2021/7/20 13:48
 */
@Repository
public interface PromoteMapper {
    /*
     * 添加促销基础信息
     * */
    int insertPromote(Promote promote);

    /*
     *查询促销基础信息
     * */
    List<Promote> queryAll();
}
